﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CalculatorMVC.Models
{
    public class CalculatorModel
    {
        [Required]
        public int FirstNumber { get; set;}

        [Required]
        public int SecondNumber { get; set;}
        public int Result { get; set;}
    }
}
