﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CalculatorMVC.Models;
using Microsoft.AspNetCore.Mvc;

namespace CalculatorMVC.Controllers
{
    public class CalculatorController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult handleAddition(CalculatorModel data, string submitButton) 
        {
            switch (submitButton)
            {
                case "add" :
                    data.Result = data.FirstNumber + data.SecondNumber;
                    return View("Index", data);
                    break;

                case "sub":
                    data.Result = data.FirstNumber - data.SecondNumber;
                    return View("Index", data);
                    break;

                case "div":
                    data.Result = data.FirstNumber / data.SecondNumber;
                    return View("Index", data);
                    break;

                default:
                    data.Result = data.FirstNumber * data.SecondNumber;
                    return View("Index", data);
            }
            
        }

        public IActionResult handleSubstraction(CalculatorModel data) 
        {
            data.Result = data.FirstNumber - data.SecondNumber;
            return View("Index", data);

        }

        public IActionResult handleDivision(CalculatorModel data) 
        {
            data.Result = data.FirstNumber / data.SecondNumber;
            return View("Index", data);
        }

        public IActionResult handleMultiplication(CalculatorModel data)
        {
            data.Result = data.FirstNumber * data.SecondNumber;
            return View("Index", data);
        }
    }
}
